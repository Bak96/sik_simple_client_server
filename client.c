#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <endian.h>
#include <inttypes.h>


#define BUFFER_LENGTH 66000
#define DEFAULT_PORT_NUMBER "20160"

struct __attribute__((__packed__)) DataHeader {
	uint64_t timestamp;
	char c;
};

//return error
int S64(const char *s, uint64_t *result) {
  uint64_t i;
  char c ;
  int scanned = sscanf(s, "%" SCNu64 "%c", &i, &c);
  *result = i;
  //OK
  if (scanned == 1) {
  	return 1;
  }
  else {
  	return 0;	
  }
  
}

int isNumber(char *s) {
	int i = 0;

	while (s[i] != 0) {
		if (s[i] < '0' || s[i] > '9') {
			return 0;
		}
		i++;
	}

	return 1;
}
//./client timestamp c host [port]
void validateArguments(int argc, char* argv[]) {
	uint64_t timestamp;
	uint64_t port;
	if (argc != 5 && argc != 4) {
		fprintf(stderr, "Usage: %s timestamp c host [port]\n", argv[0]);
		exit(1);
	}

	if (!isNumber(argv[1])) {
		fprintf(stderr, "timestamp is not a positive number\n");
		exit(1);
	}

	if (argc == 5) {
		if (!isNumber(argv[4])) {
			fprintf(stderr, "port has to be a number\n");
			exit(1);
		}
		if (!S64(argv[4], &port)) {
			fprintf(stderr, "cant parse port\n");
			exit(1);
		}
		if (port > 65535) {
			fprintf(stderr, "port has to be 0-65535\n");
			exit(1);
		}
	}

	if (!S64(argv[1], &timestamp)) {
		fprintf(stderr, "cant parse timestamp\n");
		exit(1);
	}

	if (strlen(argv[2]) != 1) {
		fprintf(stderr, "cant parse character\n");
		exit(1);
	}
}

int main(int argc, char* argv[]) {
	int sock;
	char *port;
	char buffer[BUFFER_LENGTH];
	struct addrinfo hints, *servinfo, *p;
	char s[INET_ADDRSTRLEN];
	size_t len;
	ssize_t snd_len, rcv_len;
	int flags, sflags;
	int rv;

	struct DataHeader data;
	uint64_t timestamp;
	char c;

	validateArguments(argc, argv);

	S64(argv[1], &timestamp);
	c = argv[2][0];

	if (argc == 5)
		port = argv[4];
	else
		port = DEFAULT_PORT_NUMBER;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo(argv[3], port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			fprintf(stderr, "client: socket");
			continue;
		}

		if (connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			fprintf(stderr, "client: connect");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		exit(1);
	}

	inet_ntop(p->ai_family, &(((struct sockaddr_in *)p->ai_addr)->sin_addr),
		s, sizeof s);

	printf("client: connecting to %s\n", s);
	freeaddrinfo(servinfo);

	memset(buffer, 0, sizeof(buffer));
	buffer[0] = 'a';
	
	sflags = 0;

	data.timestamp = htobe64(timestamp);
	data.c = c;
	len = sizeof(data);
	snd_len = send(sock, &data, len, sflags);

	if (snd_len != (ssize_t) len) {
		perror("error on sending datagram to server socket");
	}

	int counter = 1;
	while (1) {
		memset(buffer, 0, sizeof(buffer));
		flags = 0;
		len = (size_t) sizeof(buffer) - 1;
		rcv_len = recv(sock, buffer, len, flags);

		memcpy(&data, buffer, sizeof(data));
		data.timestamp = be64toh(data.timestamp);

		if (rcv_len < 0) {
			perror("read");
			continue;
		}
		printf("%" PRIu64 " %c ", data.timestamp, data.c);
		printf("%s\n", buffer + sizeof(data));
		counter++;
	}

}
