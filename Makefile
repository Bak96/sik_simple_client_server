TARGET: server client

CC	= gcc
CFLAGS	= -Wall -O2
LFLAGS	= -Wall

server: server.o
	$(CC) $(LFLAGS) $^ -o $@

client: client.o 
	$(CC) $(LFLAGS) $^ -o $@

.PHONY: clean TARGET
clean:
	rm -f server client *.o *~ *.bak
