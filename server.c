#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <endian.h>
#include <inttypes.h>


#define STDIN 0 // file descriptor for standard input
#define BUFFER_LENGTH 10
#define FILE_BUFFER_LENGTH 66600
#define MAX_CONNECTED_CLIENTS 42
#define MAX_AWAITING_CLIENTS 4096
#define CONNECTION_TIME 120 // in seconds
#define HEADER_SIZE 9

int S64(const char *s, uint64_t *result) {
  uint64_t i;
  char c ;
  int scanned = sscanf(s, "%" SCNu64 "%c", &i, &c);
  *result = i;
  //OK
  if (scanned == 1) {
  	return 1;
  }
  else {
  	return 0;	
  }
  
}

int isNumber(char *s) {
	int i = 0;

	while (s[i] != 0) {
		if (s[i] < '0' || s[i] > '9') {
			return 0;
		}
		i++;
	}

	return 1;
}

struct __attribute__((__packed__)) DataHeader {
	uint64_t timestamp;
	char c;
};

struct ClientObject {
	struct sockaddr_in addr;
	int active;
	time_t accept_time;
	struct DataHeader header;
};

struct ClientQueue {
	struct ClientObject *clients;
	int start;
	int num_of_el;
	int max_size;
};

void printActiveClients(struct ClientObject* clients) {
	int i;
	char remoteIP[INET_ADDRSTRLEN];
	int counter;
	time_t current_time;

	current_time = time(NULL);

	printf("printing active clients...\n");
	counter = 0;
	for (i = 0; i < MAX_CONNECTED_CLIENTS; i++) {
		if (clients[i].active) {
			counter++;
			printf("active client: %s:%d connected %ld seconds ago\n",
				inet_ntop(clients[i].addr.sin_family, &clients[i].addr.sin_addr,
								remoteIP, INET_ADDRSTRLEN), ntohs(clients[i].addr.sin_port),
								current_time - clients[i].accept_time);
		}
	}
	printf("%d active clients\n", counter);

}

void client_queue_init(struct ClientQueue *client_queue, struct ClientObject *clients, int max_size) {
	client_queue->clients = clients;
	client_queue->start = 0;
	client_queue->num_of_el = 0;
	client_queue->max_size = max_size;
}

void client_queue_push(struct ClientQueue *cq, struct ClientObject *client) {
	int pos = (cq->start + cq->num_of_el) % cq->max_size;
	cq->clients[pos] = *client;
	
	if (cq->num_of_el == cq->max_size) {
		cq->start = (cq->start + 1) % cq->max_size;
	}

	cq->num_of_el++;

	if(cq->num_of_el > cq->max_size) {
		cq->num_of_el = cq->max_size;
	}
}

struct ClientObject client_queue_pop(struct ClientQueue *cq) {
	int pos = cq->start;

	cq->start = (cq->start + 1) % cq->max_size;
	cq->num_of_el--;
	if (cq->num_of_el < 0) {
		cq->num_of_el = 0;
	}

	return cq->clients[pos];
}

int client_queue_is_empty(struct ClientQueue *cq) {
	if (cq->num_of_el == 0) {
		return 1;
	} else {
		return 0;
	}
}

int clients_equal(struct ClientObject *c1, struct ClientObject *c2) {

	if ((c1->addr.sin_addr.s_addr == c2->addr.sin_addr.s_addr)
		&& c1->addr.sin_port == c2->addr.sin_port) {
		return 1;
	}
	else {
		return 0;
	}
}

//DODAJE DO KOLEJKI OCZEKUJACYCH
void addAwaitingClient(struct ClientQueue* cq, struct sockaddr_in *their_address,
		struct DataHeader *data_header) {
	//MOZE TUTAJ WALIDACJE STRZELIC
	struct ClientObject new_client;
	new_client.addr = *their_address;
	new_client.active = 0;
	new_client.header = *data_header;
	new_client.accept_time = time(NULL);

	client_queue_push(cq, &new_client);
}

void deactivateOldClients(struct ClientObject *connected_clients, time_t time) {
	int i;
	time_t dt;
	for (i = 0; i < MAX_CONNECTED_CLIENTS; i++) {
		dt = time - connected_clients[i].accept_time;
		if(connected_clients[i].active && dt > CONNECTION_TIME) {
			connected_clients[i].active = 0;
		}
	}
}

//zwraca pozycje w tablicy
int connect_client(struct ClientObject *connected_clients, struct ClientObject *new_client) {
	int i;

	deactivateOldClients(connected_clients, new_client->accept_time);

	//sprawdze najpierw czy juz jest i jesli jest, to uaktualnie mu czas
	for (i = 0; i < MAX_CONNECTED_CLIENTS; i++) {
		if (clients_equal(&connected_clients[i], new_client)
			&& connected_clients[i].active) {
			connected_clients[i] = *new_client;		
			connected_clients[i].active = 1;	
			return i;
		}
	}

	//zajme pierwsze lepsze miejsce
	for (i = 0; i < MAX_CONNECTED_CLIENTS; i++) {
		if (!connected_clients[i].active) {
			connected_clients[i] = *new_client;
			connected_clients[i].active = 1;
			return i;
		}
	}

	return -1;
}

int createBindSocket(const char* port) {
	int sock;
	int rv;
	struct addrinfo hints, *ai, *p;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	if ((rv = getaddrinfo(NULL, port, &hints, &ai)) != 0) {
		fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
		exit(1);
	}

	for (p = ai; p != NULL; p = p->ai_next) {
		sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (sock < 0) {
			continue;
		}

		if (bind(sock, p->ai_addr, p->ai_addrlen) < 0) {
			close(sock);
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "selectserver: failed to bind\n");
		exit(1);
	}
// 	fprintf(stderr, "port: %d\n",ntohs(((struct sockaddr_in*)(p->ai_addr))->sin_port));
	freeaddrinfo(ai);
	return sock;
}

size_t prepareSendDatagram(char *filename, char *buffer, int buffer_length, int offset) {
	FILE *file;
	ssize_t file_len;
	file = fopen(filename, "rb");
	if (file == NULL) {
		fprintf(stderr, "Error: Cant open file %s\n", filename);
		exit(1);
	}

	file_len = fread(buffer + offset, sizeof(char), buffer_length - offset, file);

	fclose(file);

	return file_len;
}

void initConnectedClients(struct ClientObject *clients, int buffer_size) {
	int i;
	
	for (i = 0; i < buffer_size; i++) {
		clients[i].active = 0;
	}
}

void validateArguments(int argc, char *argv[]) {
	uint64_t port;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s port file\n", argv[0]);
		exit(1);
	}

	if (!isNumber(argv[1])) {
			fprintf(stderr, "port has to be a number\n");
			exit(1);
		}
		if (!S64(argv[1], &port)) {
			fprintf(stderr, "cant parse port\n");
			exit(1);
		}
		if (port > 65535) {
			fprintf(stderr, "port has to be 0-65535\n");
			exit(1);
		}
}

int main(int argc, char *argv[]) {
	fd_set master;	//sockety ktorych uzywamy
	fd_set read_fds, write_fds;	//sockety gotowe do czytania/pisania
	fd_set *read_fds_ptr, *write_fds_ptr;
	char buffer[BUFFER_LENGTH];	//bufor do odbierania danych
	char sendBuffer[FILE_BUFFER_LENGTH];	//bufor do ktorego ladujemy zawartosc pliku

	char remoteIP[INET_ADDRSTRLEN];	//sluzy tylko do printowania nadchodzacych polaczen
	struct ClientObject connected_clients[MAX_CONNECTED_CLIENTS]; //obslugiwani klienci
	struct ClientObject awaiting_clients[MAX_AWAITING_CLIENTS]; //czekajace w buforze datagramy
	struct ClientQueue client_queue;	//kolejka ktora obsluguje pobieranie i wstawianie danych do bufora datagramow
	struct ClientObject client_object;	//temp obj
	client_queue_init(&client_queue, awaiting_clients, MAX_AWAITING_CLIENTS); 
	
	int sock;
	int rcv_flags, snd_flags;
	
	struct sockaddr_in their_address;
	ssize_t rcv_len, snd_len;
	ssize_t file_len;
	socklen_t rcva_len;
	struct DataHeader data_header;

	int served_client = 0; //index w tablicy connected_clients ktorego zadanie jest obslugiwane
	int serving_client = 0; //jestesmy w trakcie obslugiwania klienta 0 - nie wpp - tak
	int serving_counter = 0; //na ktorym indeksie connected_clients jestesmy przy wysylaniu aktualnego datagramu

	validateArguments(argc, argv);
	sock = createBindSocket(argv[1]);
	initConnectedClients(connected_clients, MAX_CONNECTED_CLIENTS);
	file_len = prepareSendDatagram(argv[2], sendBuffer, FILE_BUFFER_LENGTH, HEADER_SIZE);

	FD_SET(sock, &master);

	rcv_flags = 0;
	snd_flags = 0;
	rcva_len = (socklen_t) sizeof(their_address);

	for(;;) {
		//jesli nie obslugujemy datagramow, to trzeba zaczac
		if (!serving_client && !client_queue_is_empty(&client_queue)) {
			client_object = client_queue_pop(&client_queue);
			served_client = connect_client(connected_clients, &client_object);
			serving_client = 1;
			//printActiveClients(connected_clients);
		}

		read_fds = master;
		write_fds = master;

		read_fds_ptr = &read_fds;
		//wiekszosc czasu write bedzie dostepny, a chcemy go tylko kiedy obslugujemy klienta
		if (serving_client)
			write_fds_ptr = &write_fds;
		else
			write_fds_ptr = NULL;

		if (select(sock+1, read_fds_ptr, write_fds_ptr, NULL, NULL) == -1) {
			fprintf(stderr, "Error: select");
			exit(1);
		}


		if (FD_ISSET(sock, &read_fds)) {
			memset(buffer, 0, BUFFER_LENGTH);

			rcv_len = recvfrom(sock, buffer, sizeof(buffer), rcv_flags,
				(struct sockaddr *) &their_address, &rcva_len);
			if (rcv_len < 0) {
				fprintf(stderr, "error on receiving datagram\n");
			}
			else if (rcv_len != 9) {
				fprintf(stderr, "server: bledny datagram od %s:%d\n",
					inet_ntop(their_address.sin_family, &their_address.sin_addr,
						remoteIP, INET_ADDRSTRLEN),ntohs(their_address.sin_port));
			}
			else {
/*
				printf("server: message from %s:%d\n",
					inet_ntop(their_address.sin_family, &their_address.sin_addr,
						remoteIP, INET_ADDRSTRLEN),ntohs(their_address.sin_port));

				printf("read from socket: %zd bytes: %s\n", rcv_len, buffer);
				//POPORAWNY PAKIET NA WEJSCIU POSTARAJ SIE GO DODAC
*/
				memcpy(&data_header, buffer, sizeof(data_header));
				data_header.timestamp = be64toh(data_header.timestamp);
				addAwaitingClient(&client_queue, &their_address, &data_header);
			}
		}//END IF(FD_ISSET(i, &read_fds))

		//wysylam pakiety
		if (FD_ISSET(sock, &write_fds)) {
			if (serving_client) {
				data_header = connected_clients[served_client].header;
				data_header.timestamp = htobe64(data_header.timestamp);

				memcpy(sendBuffer, &data_header, sizeof(data_header));

				//omijam tych ktorym nie wysle
				for (serving_counter = serving_counter;
					serving_counter < MAX_CONNECTED_CLIENTS;
					serving_counter++) {

					if (connected_clients[serving_counter].active 
					&& !clients_equal(&connected_clients[served_client], 
							&connected_clients[serving_counter])) {
						break;
					}	
				}
				
				if (serving_counter != MAX_CONNECTED_CLIENTS) {
					snd_len = sendto(sock, sendBuffer, (size_t) file_len + HEADER_SIZE, snd_flags,
					(struct sockaddr *) &connected_clients[serving_counter].addr, rcva_len);

					printf("server: send %zd bytes to %s:%d\n", snd_len,
						inet_ntop(connected_clients[serving_counter].addr.sin_family, &connected_clients[serving_counter].addr.sin_addr,
					remoteIP, INET_ADDRSTRLEN),ntohs(connected_clients[serving_counter].addr.sin_port));

					if (snd_len != file_len + HEADER_SIZE)
						fprintf(stderr, "error on sending datagram to client socket");

					serving_counter++;
				}
				else { //wyslalismy juz wszystkim
					serving_counter = 0;
					serving_client = 0;
				}
			
			}				
		}
	} //END SERVER LOOP
}